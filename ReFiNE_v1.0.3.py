#!/usr/bin/python

"""
ReFiNE - Reducing False-positives in NGS Elucidation
Detailed Instructions found in the readme.txt provided with this script


Required arguments:
: cutoff 			minimum frequency for a variant to be included in blacklist; valid range 0.0 to 1.0

: path_to_exomes 	folder that contains exome files; files must be in .csv format and include VCF-style variant
					information (Chrom, Pos, ID, Ref, Alt)

Optional arguments:
: gui				launch graphical interface (must be set as first argument; no other arguments permitted)

: ptlist 			path to a .txt file containing patient numbers to use for blacklist generation; the blacklist
					generator will look for these patient exomes in the path_to_exomes location; each patient number
					should be on its own line ('\n' separated)

: exclude 			path to a .txt file containing variations in tab-separated VCF format that should be excluded from
					blacklist generation; useful if you have a set of known disease-causing variants in your cohort or
					if you have access to private variant databases, such as HGMD

: minimum_pts 		minimum number of patients that must have a variant for it to be included in blacklist; useful for
					small cohorts where your cutoff frequency may represent only one or two patients; default = 2
: dp				minimum read depth for a variant to be included in the blacklist; default = 4; DP column name MUST
					be "DP"
: mq				minimum MQ score for a variant to be included in the blacklist; default = 20; MQ column name MUST
					be "MQ"

Examples:
    ReFiNE_v1.0.3.py -gui
    	-start blacklist generator in GUI (Graphical User Interface) mode

    ReFiNE_v1.0.3.py 0.03 ~/Desktop/Exomes
    	-create blacklist with cutoff frequency of 3%; folder containing exomes is located on user's desktop

    ReFiNE_v1.0.3.py 0.1 /Volumes/PatientExomes/ -ptlist ~/Desktop/patient_ids.txt
    	-create blacklist with cutoff frequency of 10%; folder containing exomes is located on an external drive named
    		'PatientExomes'; instead of using all exomes in the drive, only use patient IDs listed in the file
    		'patient_ids.txt' located on the user's desktop

    ReFiNE_v1.0.3.py 0.01 ~/Desktop/Exomes -exclude ~/Desktop/excluded_vars.txt -minimum_pts 5
    	-create blacklist with cutoff frequency of 1%; folder containing exomes is located on user's desktop; exclude
    		variations listed in the "excluded_vars.txt" file on the user's desktop from the blacklist; at least 5
    		patients must have any variant for it to be included in the blacklist

	ReFiNE_v1.0.3.py 0.01 ~/Desktop/Exomes -dp 10 -mq 40
		-create blacklist with cutoff frequency of 1%; folder containing exomes is located on user's desktop; set
		minimum DP and MQ scores for a variant to be considered as 10 and 40, respectively

Please address questions, comments, and bug reports to patrick.maffucci@icahn.mssm.edu

Copyright 2017,2018 - Patrick Maffucci
"""

import csv
import glob
import os
import sys
import argparse
import thread

import Tkinter as tk
import tkFileDialog
from tkFont import Font
from tkMessageBox import showinfo, showwarning

from datetime import datetime

# import cPickle as pickle

########################################################################################################################
# Define Functions                                                                                                     #
########################################################################################################################


def check_cutoff(x):
	if float(x) < 0.0 or float(x) > 1.0:
		raise argparse.ArgumentTypeError('{0} is not in range (0.0, 1.0).'.format(x))
	return float(x)


def check_filetype(x):
	if not x == 'csv':
		raise argparse.ArgumentTypeError('{0} is not a valid file type. Please use "csv".'.format(x))
	return str(x)


def directory(y, ft):
	if os.path.isdir(y) is False:
		raise argparse.ArgumentTypeError('Directory "{0}" does not exist.'.format(y))
	elif not any(fle.endswith('.{0}'.format(ft)) for fle in os.listdir(y)):
		raise argparse.ArgumentTypeError('Directory "{0}" does not contain exomes in {1} format.'.format(y, ft))
	return y


def sort_variations(variation, variationdict):
	if variation not in variationdict:
		variationdict[variation] = 1
	else:
		variationdict[variation] += 1
	return variationdict


def make_variation_dicts(list_of_exomes, arguments):
	# if param exclude was specified, create set of exclusion variations
	if arguments.exclude is not None:
		excluded_vars = make_exclusion_set(arguments.exclude)
	else:
		excluded_vars = None
	# create dictionaries
	vardict = {}
	# loop through exomes
	counter = 0
	for ptfile in list_of_exomes:
		try:
			with open(ptfile, 'r') as csvfile:
				ptseen = set()
				counter += 1
				sys.stdout.write('\r{0} exomes processed.'.format(counter))
				sys.stdout.flush()
				if counter == 1: # only need to do this once! all csv should have the same header
					header = [x.strip() for x in next(csvfile).split(',')]
					dpcol = header.index('"DP"')
					mqcol = header.index('"MQ"')
				else:
					next(csvfile)
				for row in csv.reader(csvfile):
					# check variant meets quality criteria
					if float(row[dpcol]) >= arguments.dp and float(row[mqcol]) >= arguments.mq:
						var = row[0] + ',' + row[1] + ',' + row[3] + ',' + row[4]
						# guard against variants accidentally repeated in an individual exome (yes, it happens...)
						if var in ptseen:
							continue
						else:
							ptseen.update([var])
						# ignore excluded variations if param exclude was specified
						if excluded_vars is not None and var in excluded_vars:
							continue
						# pass variations to sort_variations function for sorting
						else:
							vardict = sort_variations(var, vardict)
					else:
						continue
		except IOError:
			# whoops
			raise IOError('{0} file not found.'.format(ptfile))
	return vardict


def condense_positions(variationdict):
	condensed_dict = {}
	for var in variationdict:
		key = ','.join(var.split(',')[:2])
		if key not in condensed_dict:
			condensed_dict[key] = [variationdict[var], [var]]
		else:
			condensed_dict[key][0] += int(variationdict[var])
			condensed_dict[key][1].append(var)
	return condensed_dict


def make_exclusion_set(exclusion_file):
	exclusion_set = set()
	# check if file has a header
	if csv.Sniffer().has_header(exclusion_file.read(1024)) is True:
		exclusion_file.seek(0)
		next(exclusion_file)
	else:
		exclusion_file.seek(0)
	for row in csv.reader(exclusion_file, delimiter='\t'):
		ref = row[0] + ',' + row[1] + ',' + row[3] + ',' + row[4]
		exclusion_set.update([ref])
	return exclusion_set


def main(arguments):
	print '--- ReFiNE v1.0.3 ---'
	print 'Start time: {0}'.format(datetime.now())
	# Create blacklist files and write header
	os.chdir(os.path.dirname(os.path.abspath(__file__)))
	bltot = open('{0}_blacklist.txt'.format(arguments.cutoff), 'w')
	header = 'Chr\tPos\tRef\tAlt\tCondensed # Patients\tIndividual # Patients\t# Variants at Position\t' \
			 'Total # Patients\n'
	bltot.write(header)

	# Check exome folder
	directory(arguments.path_to_exomes, 'csv')

	# Generate list of exomes to use for blacklist generation
	if arguments.ptlist is not None:
		exomelist = ['{0}{1}.csv'.format(arguments.path_to_exomes, ptnum) for ptnum in
					 arguments.ptlist.read().split('\n')]
	else:
		exomelist = glob.glob(arguments.path_to_exomes + '*.csv')
	numpts = len(exomelist)

	# ##################################################################################################################
	# MAKE DICTIONARIES
	# vardict: 			dictionary of all variants in exomes and their counts
	# condensed_chrpos:	all variants in vardict collapsed to unique chromosomal positions
	print 'Processing exome files from "{0}".'.format(arguments.path_to_exomes)
	vardict = make_variation_dicts(exomelist, arguments)
	condensed_chrpos = condense_positions(vardict)

	print "\nCreating blacklist..."
	# ##################################################################################################################
	# CREATE BLACKLIST
	for chrpos in condensed_chrpos:
		if ((float(condensed_chrpos[chrpos][0]) / int(numpts)) > arguments.cutoff) and \
						condensed_chrpos[chrpos][0] >= arguments.minimum_pts:
			# the following is code is necessary to restrict the max number of patients with a variant to the total
			# number of patients in the cohort (necessary due to multi-allelic positions where some patients have more
			# than one variant)
			if int(condensed_chrpos[chrpos][0]) > int(numpts):
				sharedptnum = int(numpts)
			else:
				sharedptnum = int(condensed_chrpos[chrpos][0])
			# write blacklist to output
			for var in condensed_chrpos[chrpos][1]:
				towrite = '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\n'.format(var.split(',')[0], var.split(',')[1],
																  			var.split(',')[2], var.split(',')[3],
																  			sharedptnum, vardict[var],
																			len(condensed_chrpos[chrpos][1]), numpts)
				bltot.write(towrite)

	# be good
	bltot.close()

	# done
	print "End time: {0}".format(datetime.now())
	print "Ding!"


class BLGUI(tk.Frame):
	def __init__(self):
		tk.Frame.__init__(self, bg='grey', width=350, height=400)
		self.grid_propagate(0)
		self.grid()
		self.initUI()

	def initUI(self):
		self.lbl1 = tk.Label(self, text='Required Parameters', font=Font(weight='bold'), bg='grey')
		self.lbl1.grid(row=0, column=0, sticky=tk.W)

		self.lbl2 = tk.Label(self, text='Set frequency cutoff:', anchor=tk.W, bg='grey')
		self.lbl2.grid(row=1, column=0, sticky=tk.W)
		self.entry1 = tk.Entry(self, width=4)
		self.entry1.config(highlightbackground='grey')
		self.entry1.grid(row=1, column=4, sticky=tk.E)

		self.lbl3 = tk.Label(self, text='Exome folder:', anchor=tk.W, bg='grey')
		self.lbl3.grid(row=2, column=0, sticky=tk.W)
		self.path = tk.StringVar(self.master)
		self.entry3 = tk.Entry(self, textvariable=self.path)
		self.entry3.config(highlightbackground='grey')
		self.entry3.grid(row=3, column=0, columnspan=5, sticky=tk.W + tk.E)
		self.butt2 = tk.Button(self, text='Browse', command=self.set_exome_path)
		self.butt2.config(highlightbackground='grey')
		self.butt2.grid(row=3, column=5)

		self.lbl4 = tk.Label(self, text='', bg='grey')
		self.lbl4.grid(row=4)

		self.lbl5 = tk.Label(self, text='Optional Parameters', font=Font(weight='bold'), bg='grey')
		self.lbl5.grid(row=5, sticky=tk.W)

		self.lbl6 = tk.Label(self, text='Minimum Patient #:', bg='grey')
		self.lbl6.grid(row=6, sticky=tk.W)
		self.entry4 = tk.Entry(self, width=4)
		self.entry4.config(highlightbackground='grey')
		self.entry4.grid(row=6, column=4, sticky=tk.E)

		self.lbl7 = tk.Label(self, text='Patient list:', bg='grey')
		self.lbl7.grid(row=7, column=0, sticky=tk.W)
		self.ptlist = tk.StringVar(self.master)
		self.entry2 = tk.Entry(self, textvariable=self.ptlist)
		self.entry2.config(highlightbackground='grey')
		self.entry2.grid(row=8, column=0, columnspan=5, sticky=tk.W + tk.E)
		self.butt3 = tk.Button(self, text='Browse', command=self.set_ptlist_path)
		self.butt3.config(highlightbackground='grey')
		self.butt3.grid(row=8, column=5)

		self.lbl8 = tk.Label(self, text='Excluded Vars:', bg='grey')
		self.lbl8.grid(row=9, sticky=tk.W)
		self.exvars = tk.StringVar(self.master)
		self.entry5 = tk.Entry(self, textvariable=self.exvars)
		self.entry5.config(highlightbackground='grey', width=29)
		self.entry5.grid(row=10, column=0, columnspan=5, sticky=tk.W + tk.E)
		self.butt4 = tk.Button(self, text='Browse', command=self.set_exvars_path)
		self.butt4.config(highlightbackground='grey')
		self.butt4.grid(row=10, column=5)

		self.lbl9 = tk.Label(self, text='Minimum DP', bg='grey')
		self.lbl9.grid(row=11, sticky=tk.W)
		self.entry6 = tk.Entry(self, width=4)
		self.entry6.config(highlightbackground='grey')
		self.entry6.grid(row=11, column=4, sticky=tk.E)

		self.lbl10 = tk.Label(self, text='Minimum MQ', bg='grey')
		self.lbl10.grid(row=12, sticky=tk.W)
		self.entry7 = tk.Entry(self, width=4)
		self.entry7.config(highlightbackground='grey')
		self.entry7.grid(row=12, column=4, sticky=tk.E)

		self.butt1 = tk.Button(self, text='Run!', command=lambda: thread.start_new_thread(self.get_args, ()))
		self.butt1.config(highlightbackground='grey')
		self.butt1.grid(row=19, column=0, columnspan=6, sticky=tk.N)
		self.butt5 = tk.Button(self, text='Help', command=self.show_help)
		self.butt5.config(highlightbackground='grey')
		self.butt5.grid(row=19, column=0, columnspan=6, sticky=tk.E)

		self.lbl11 = tk.Label(self, text='Copyright 2017,2018 - Patrick Maffucci', font=Font(size='8'),
										bg='grey')
		self.lbl11.grid(row=20, column=0, columnspan=6, sticky=tk.W+tk.E)

	def show_help(self):
		helper = tk.Toplevel(self)
		helper.wm_title('Help')
		help_frame = tk.Frame(helper, bg='grey', width=625, height=570)
		help_frame.grid_propagate(0)
		help_frame.grid()

		help_label = tk.Label(help_frame, text='ReFiNE Help', bg='grey', font=Font(slant='italic'))
		help_label.grid(row=0, column=0, columnspan=2, sticky=tk.N)

		freq = tk.Label(help_frame, text='Set frequency cutoff:', bg='grey', font=Font(slant='italic',
																							  weight='bold'))
		freq2 = tk.Label(help_frame, text='Specify minimum frequency for a variant to be included in blacklists;',
						 bg='grey')
		freq3 = tk.Label(help_frame, text='Must be a decimal between 0.0 and 1.0', bg='grey')
		freq.grid(row=1, column=0, sticky=tk.E)
		freq2.grid(row=1, column=1, columnspan=2, sticky=tk.W)
		freq3.grid(row=2,column=1, columnspan=2, sticky=tk.W)

		exome = tk.Label(help_frame, text='Exome folder:', bg='grey', font=Font(slant='italic', weight='bold'))
		exome2 = tk.Label(help_frame, text='Specify directory containing patient exomes', bg='grey')
		exome.grid(row=3, column=0, sticky=tk.E)
		exome2.grid(row=3, column=1, sticky=tk.W)

		spacer3 = tk.Label(help_frame, text='', bg='grey')
		spacer3.grid(row=4)

		minpt = tk.Label(help_frame, text='Minimum pt #:', bg='grey', font=Font(slant='italic', weight='bold'))
		minpt2 = tk.Label(help_frame, text='(Optional) Minimum number of patients that must have a variant for it ',
						  bg='grey')
		minpt3 = tk.Label(help_frame, text='to be included in blacklist; useful for small cohorts where your cutoff',
						  bg='grey')
		minpt4 = tk.Label(help_frame, text='frequency may represent only one or two patients', bg='grey')
		minpt5 = tk.Label(help_frame, text='If left blank, defaults to 2', bg='grey')
		minpt.grid(row=5, column=0, sticky=tk.E)
		minpt2.grid(row=5, column=1, sticky=tk.W)
		minpt3.grid(row=6, column=1, sticky=tk.W)
		minpt4.grid(row=7, column=1, sticky=tk.W)
		minpt5.grid(row=8, column=1, sticky=tk.W)

		plist = tk.Label(help_frame, text='Patient list:', bg='grey', font=Font(slant='italic', weight='bold'))
		plist2 = tk.Label(help_frame, text='(Optional) Specify a .txt file containing patient numbers to use for',
						  bg='grey')
		plist3 = tk.Label(help_frame, text='blacklist generation; the blacklist generator will look for these patient',
						  bg='grey')
		plist4 = tk.Label(help_frame, text='exome files in the exome folder specified above; in the .txt, each patient',
						  bg='grey')
		plist5 = tk.Label(help_frame, text='number should be on a separate line', bg='grey')
		plist.grid(row=9, column=0, sticky=tk.E)
		plist2.grid(row=9, column=1, sticky=tk.W)
		plist3.grid(row=10, column=1, sticky=tk.W)
		plist4.grid(row=11, column=1, sticky=tk.W)
		plist5.grid(row=12, column=1, sticky=tk.W)

		exvar = tk.Label(help_frame, text='Excluded vars:', bg='grey', font=Font(slant='italic', weight='bold'))
		exvar2 = tk.Label(help_frame, text='(Optional) Path to a .txt file containing variations (in tab-separated',
						  bg='grey')
		exvar3 = tk.Label(help_frame, text='vcf format) that should be excluded from blacklist generationl useful',
						  bg='grey')
		exvar4 = tk.Label(help_frame, text='if you have a set of known disease-causing variants in your cohort ',
						  bg='grey')
		exvar5 = tk.Label(help_frame, text='or if you have access to private variant databases, such as HGMD',
						  bg='grey')
		exvar.grid(row=13, column=0, sticky=tk.E)
		exvar2.grid(row=13, column=1, sticky=tk.W)
		exvar3.grid(row=14, column=1, sticky=tk.W)
		exvar4.grid(row=15, column=1, sticky=tk.W)
		exvar5.grid(row=16, column=1, sticky=tk.W)
		
		dpvar = tk.Label(help_frame, text='Minimum DP:', bg='grey', font=Font(slant='italic', weight='bold'))
		dpvar2 = tk.Label(help_frame, text='(Optional) Set the minimum read depth for variants included in the',
						  bg='grey')
		dpvar3 = tk.Label(help_frame, text='blacklist; default=4', bg='grey')
		dpvar.grid(row=17, column=0, sticky=tk.E)
		dpvar2.grid(row=17, column=1, sticky=tk.W)
		dpvar3.grid(row=18, column=1, sticky=tk.W)
		
		mqvar = tk.Label(help_frame, text='Minimum MQ:', bg='grey', font=Font(slant='italic', weight='bold'))
		mqvar2 = tk.Label(help_frame, text='(Optional) Set the minimum MQ score for variants included in the',
						  bg='grey')
		mqvar3 = tk.Label(help_frame, text='blacklist; default=20', bg='grey')
		mqvar.grid(row=19, column=0, sticky=tk.E)
		mqvar2.grid(row=19, column=1, sticky=tk.W)
		mqvar3.grid(row=20, column=1, sticky=tk.W)
		
		spacer1 = tk.Label(help_frame, text='', bg='grey')
		spacer1.grid(row=21)

		corr = tk.Label(help_frame, text='Please address questions, comments, and bug reports to '
										 'patrick.maffucci@icahn.mssm.edu',
						bg='grey')
		corr.grid(row=22, column=0, columnspan=2, sticky=tk.N)

		spacer7 = tk.Label(help_frame, text='', bg='grey')
		spacer7.grid(row=23)

		butt_done = tk.Button(help_frame, text='Done', command=helper.destroy)
		butt_done.config(highlightbackground='grey')
		butt_done.grid(row=24, column=0, columnspan=2, sticky=tk.N)

	def set_exome_path(self):
		self.path.set(tkFileDialog.askdirectory(parent=self.master))

	def set_ptlist_path(self):
		self.ptlist.set(tkFileDialog.askopenfilename(parent=self.master))

	def set_exvars_path(self):
		self.exvars.set(tkFileDialog.askopenfilename(parent=self.master))

	def check_float(self, floatval):
		try:
			val = float(floatval)
			if val < 0.0 or val > 1.0:
				showwarning('Error!', 'Cutoff value "{0}" not a floating number between 0.0 and 1.0!\n\n'
									  'Please enter a value between 0.0 and 1.0'.format(floatval),
							parent=self.master)
			else:
				return True
		except ValueError:
			showwarning('Error!', 'Cutoff value "{0}" not a floating number!\n\nPlease enter a value '
											 'between 0.0 and 1.0!'.format(floatval), parent=self.master)

	def check_exome_dir(self, dir):
		if os.path.isdir(dir) is False:
			showwarning('Error!', '"{0}" is not a directory or doesn\'t exist!'.format(dir),
									 parent=self.master)
		elif not any(fle.endswith('.csv') for fle in os.listdir(dir)):
			showwarning('Error!', 'Directory "{0}" does not contain exomes in CSV format!'.format(dir),
									 parent=self.master)
		else:
			return True

	def get_args(self):
		win = tk.Toplevel(self)
		win_frame = tk.Frame(win, bg='grey', width=250, height=145)
		win_frame.grid_propagate(0)
		win_frame.grid()
		win.title('Running...')
		tk.Label(win_frame, text='    ', bg='grey').grid(row=0, column=0)
		tk.Label(win_frame, text='Generator running!', bg='grey').grid(row=1, column=1, sticky=tk.N)
		tk.Label(win_frame, text='', bg='grey').grid(row=2)
		tk.Label(win_frame, text='You will receive a notification', bg='grey').grid(row=3, column=1, sticky=tk.N)
		tk.Label(win_frame, text='when the process is complete.', bg='grey').grid(row=4, column=1, sticky=tk.N)
		if self.check_float(self.entry1.get()) is True and self.check_exome_dir(self.entry3.get()) is True:
			self.cutoff = self.entry1.get()
			if str(self.entry3.get()).endswith('/'):
				self.path_to_exomes = str(self.entry3.get())
			else:
				self.path_to_exomes = str(self.entry3.get()) + '/'
			if self.entry4.get():
				self.minimum_pts = int(self.entry4.get())
			else:
				self.minimum_pts = int(2)
			if self.entry2.get():
				self.ptlist = open(self.entry2.get(), 'r')
			else:
				self.ptlist = None
			if self.entry5.get():
				self.exvars = open(self.entry5.get(), 'r')
			else:
				self.exvars = None
			if self.entry6.get():
				self.dp = int(self.entry6.get())
			else:
				self.dp = int(4)
			if self.entry7.get():
				self.mq = int(self.entry7.get())
			else:
				self.mq = int(20)
			self.args = argparse.Namespace(cutoff=float(self.cutoff), path_to_exomes=self.path_to_exomes,
										   minimum_pts=int(self.minimum_pts), ptlist=self.ptlist, exclude=self.exvars,
										   gui=True, dp=int(self.dp), mq=int(self.mq))
			main(self.args)
			win.destroy()
			self.bl_done()
			self.quit()
		else:
			self.initUI()

	def bl_done(self):
		showinfo(title='Process Complete', message='Blacklist Generated!', parent=self.master)

########################################################################################################################
# Main                                                                                                                 #
########################################################################################################################


if __name__ == '__main__':
	# Create GUI or parse command-line options
	num_args = len(sys.argv)
	if num_args == 2 and sys.argv[1] == '-gui':
		blapp = BLGUI()
		blapp.master.title('ReFiNE v1.0.3')
		# center window
		ws = blapp.winfo_screenwidth()
		hs = blapp.winfo_screenheight()
		x = (ws / 2) - (350 / 2)
		y = (hs / 2) - (350 / 2)
		blapp.master.geometry('{0}x{1}+{2}+{3}'.format(350, 375, x, y))
		# run gui
		blapp.mainloop()
	else:
		parser = argparse.ArgumentParser(description='Create variation blacklists')
		parser.add_argument('cutoff', type=check_cutoff, help='Floating value for cutoff frequency (0.0 to 1.0)')
		parser.add_argument('path_to_exomes', type=str, help='Path to folder containing exomes')
		parser.add_argument('-gui', help='Start blacklist generator in GUI mode; do not specify any other arguemnts')
		parser.add_argument('-ptlist', type=file,
							help='List of patients used to generate blacklists instead of all files in '
								 'path_to_exomes directory; Accepts "\\n" separated files')
		parser.add_argument('-exclude', type=file,
							help='List of variations in vcf format (Chrom,Pos,ID,Ref,Alt) to exclude '
								 'from blacklists; Accepts tab separated files')
		parser.add_argument('-minimum_pts', type=int, default=2,
							help='Minimum number of patients with a variation to be '
								 'included in blacklist (useful for small cohorts); '
								 'default = 2')
		parser.add_argument('-dp', type=int, default=4, help='Minimum read depth required for a variant to be included '
							'in the blacklist; default = 4')
		parser.add_argument('-mq', type=int, default=20, help='Minimum MQ score required for a variant to be included '
							'in the blacklist; default = 20')
		args = parser.parse_args()

		# run main()
		main(args)

########################################################################################################################
# End Main                                                                                                             #
########################################################################################################################
